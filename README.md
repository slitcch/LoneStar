# LoneStar
Hello! This is the homepage of Lone Star, an offshoot of [Project North Star](https://project-north-star.gitbook.io/project-north-star/). 

## Designed in Blender
Blender, while not really optimized for CAD, is open-source, and has a lot less friction than any CAD program I have tried, which (in my opinion) allows people to make mods a lot more easily. Also, using help from Blender Cycles, one can easily reposition the screens for any arbitrary focal distance and get a good result. This might also come in handy for a virtual calibration, done entirely inside the computer. I have not personally explored using Cycles that much; it remains to be seen exactly what Cycles can help us with.

Of course, using Blender comes with the downside that it's not as powerful - there are quite a few things that "real" CAD can do that Blender can't, BRep and parametric modeling being notable examples. Not being able to use BRep really stinks sometimes, but the Bevel, Subsurf, Solidify and Boolean modifiers can help approximate BRep. I personally think the advantages (low friction, ease of use for people like me who are good at 3D modeling but bad at CAD, Blender Cycles to help with optics) outweigh the disadvantages.



# Rest of the readme, which is now very outdated, but maybe interesting to see what I was thinking four months ago. Some day I will make a new writeup for it, but not today.
## Weight reductions
### Removing retaining bars
I got rid of these retaining bars that hold the combiner in:
![](readme_files/badbars.png "They don't work too well imo")


and replaced them with a single ziptie:

![](readme_files/goodziptie.png)


The entire bottom part of the optics bracket could be removed because of this.

![](readme_files/fullgood.png) 


Those retaining bars were a pain. They were hard to print - often causing print failures; and they were weak, liable to break.

This change saves a whole lot of weight and printing, removes one point of failure, gives you more peripheral vision, and feels a lot more open in general. 

## No Leap Motion Controller or Rigel
Leap Motion/Ultraleap is a very good company that has done a lot to support open-source software, and I like them. However, their last software release for Linux was six years ago. While I did get it to *work* on Linux, the result was not good enough for AR/VR. And I'm a huge Linux guy, and there's no way in hell I'm going to use this headset only on Windows. I've been working on fiducial-based controller tracking, which has been working much better so far, and is indeed more versatile if I want to play VR games. Soon I'll probably have a link to a repo for my fiducial tracking system. Not yet though.

So, there is no reason for me to include the Leap Motion Controller in the design: it's just dead weight that I'm not going to use. If you want to use the LMC or Rigel in this design, feel free to fork this repo and add it in. *If Ultraleap/Leap Motion releases new Linux software, I will very likely add the Rigel or LMC into this design.*

## Removed seperate optics trays.
The old design had optics trays seperate from the main optics bracket. 
![](readme_files/oldtrays.png) 

These added weight, extra printing, and variance between different prints.

My design includes the trays as part of the optics bracket, and uses a single screw to retain the screens.

![](readme_files/backtrays.png) 

 Also, the bracket doesn't fully cover the backs of the screens:

![](readme_files/ohno.png) 

which doesn't protect the screens as well, but reduces weight.

All this weight reduction means that the optics bracket is a ~7 hour print, compared to a 20-hour print for Deck X/3.2. I think this balances the disadvantage of one focal length per bracket: it doesn't take that long to just print a bracket with a different focal length. (I have yet to design a bracket with a different focal length, but it shouldn't be that hard.)

## Optical changes
The screens have been moved back a bit compared to v3.2/Deck X 75cm. I haven't done the math yet, but my guess is the focal distance is between 75cm and 125cm. This is less optimized for close hand-based interactions like the v3.2/Deck X seemed to be, and more optimized for general AR/VR where objects might be seen very far away. This design should also be a lot easier on people's eyes, and give people less myopia.

## You probably shouldn't use this yet: notable issues
This is in an **extremely** early state - not really ready for public consumption. Some parts are very rough designs, or not designed at all.

Nothing is professional-looking. Looks exactly like something a 16-year-old would come up with in his parents' garage. Surprising.

No protection for expensive parts like the T261

I don't really like the positioning of the display driver board

I don't own the Deck X's integrator board, instead I'm using some USB hub I found in my house. The headset needs three cables - one for power, one for USB, and one for DisplayPort. Also the hub I have is unreasonably big/heavy. *If somebody wants to send me a Deck X integrator board, I would definitely use it and include it in the official design*

Mounting system from the welding headgear to the headset is not a good design on my part; needs to be fixed

uses the t261, which isn't open-source. Want to switch to OAK-D

Haven't designed a calibration mount for this; just using an old calibration I have lying around from 3.2. **This is a major problem, and I have to fix this before I can recommend anybody uses this design.**

BOM isn't finished and no build instructions. **This is a major problem, and I have to fix this before I can recommend anybody uses this design.**

## Contributors / attributions
Moses Turner created this, with input and ideas from [HyperLethalVector](https://github.com/HyperLethalVector), [Bryan](https://github.com/BryanChrisBrown) and [Nova](https://github.com/technobaboo), and a lot of inspiration from [this page](http://konsole.co/pns-konsole). The original design (which this is VERY derivative of) was created by Leap Motion.